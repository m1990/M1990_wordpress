<?php
/*
Template Name: 友情链接
*/
?>

<?php get_header(); ?>
      <header>
        <h1><?php the_title(); ?></h1>
      </header>
      <nav>
        <span><a title="home page" class="" href="/">home</a></span>
        <?php get_sidebar(); ?>
		<span><a title="link page" class="" href="/link">Friend</a></span>
      </nav>
      <article class="content">
        <section class="post">
			<div class="link-content">
				<ul>
				<?php wp_list_bookmarks('title_li=&categorize=0&show_images=1'); ?>
				</ul>
			</div>
        </section>
        
        <section class="comment">
          <div id="disqus_thread"></div>
            <div class="cm-article" data-key="<?php the_ID(); ?>"></div>
        </section>
      </article>
    <link rel="stylesheet" href="//comment.moe/dest/static/css/plus.css">
    <script src="//comment.moe/dest/static/js/build.js" charset="UTF-8"></script>
<?php get_footer(); ?>