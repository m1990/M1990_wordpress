<?php
    $terms = get_terms('category', 'orderby=name&hide_empty=0' );
    $count = count($terms);
    if($count > 0){
        foreach ($terms as $term) {
            echo '<span><a href="'.get_term_link($term, $term->slug).'" title="'.$term->name.'">'.$term->name.'</a><span>';
        }
    }
?>