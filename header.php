<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="en" lang="en-us">

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="Zhao Li" />
  <title><?php if ( is_home() ) {
        bloginfo('name'); echo " - "; bloginfo('description');
    } elseif ( is_category() ) {
        single_cat_title(); echo " - "; bloginfo('name');
    } elseif (is_single() || is_page() ) {
        single_post_title();
    } elseif (is_search() ) {
        echo "搜索结果"; echo " - "; bloginfo('name');
    } elseif (is_404() ) {
        echo '页面未找到!';
    } else {
        wp_title('',true);
    } ?></title>
<?php
if (is_home()){	
    $description = "这里是主页描述";	
    $keywords = "这里是主页关键词";}
    elseif(is_single()){	
        if($post->post_excerpt){
            $description = $post->post_excerpt;
        }	
        elseif(function_exists('wp_thumbnails_excerpt')){
            $description = wp_thumbnails_excerpt($post->post_content, true);
        }	
        else {
            $description = $post->post_title;
        }		
        $keywords = "";		
        $tags = wp_get_post_tags($post->ID);	
        foreach ($tags as $tag ){
            $keywords = $keywords . $tag->name  . ",";
        }
        $keywords = rtrim($keywords, ', ');
    }
    elseif(is_category()){	
        $keywords = single_cat_title('', false);	
        $description = category_description();
    }
    elseif (is_tag()){	
        $keywords = single_tag_title('', false);	
        $description = tag_description();
    }
    elseif (is_page('page_name')){	
        $keywords = "这里是关键词";	
        $description = "这里是描述";
    }
echo "<meta name=\"description\" content=\"$description\" /><meta name=\"keywords\" content=\"$keywords\" />";
?>
  <link rel="shortcut icon" href="/favicon.ico">
  <link href="https://livc.io/feed/" rel="alternate" title="Zhao Li" type="application/atom+xml" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/highlight.css">
</head>

<body>
  <div id="MathJax_Message" style="display: none;"></div>
  <div id="container">
    <div id="main" role="main">