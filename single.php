<?php get_header(); ?>
      <header>
        <h1><?php the_title(); ?></h1>
      </header>
      <nav>
        <span><a title="home page" class="" href="/">home</a></span>
        <?php get_sidebar(); ?>
		<span><a title="link page" class="" href="/link">Friend</a></span>
      </nav>
      <article class="content">
        <section class="post">
            <?php if (have_posts()) : the_post(); update_post_caches($posts); ?>
                <?php the_content(); ?>
            <?php else : ?>
                <h2>文章不存在</h2>
            <?php endif; ?>
        </section>
        <section class="meta">
          <span class="author">作者：<a href="#"><?php echo the_author_meta( 'user_nicename' );?></a> / <?php the_time('Y-n-j') ?>&nbsp;</span>
          <br/>
          <span class="categories">分类：<?php the_category(', ') ?>&nbsp;</span>
          <span class="tags"><?php the_tags('标签：', ', ', ''); ?>&nbsp;</span>
          <span class="license">Published under <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/3.0/">(CC) BY-NC-SA</a>&nbsp;</span>
        </section>
        
        <section class="comment">
          <div id="disqus_thread"></div>
            <div class="cm-article" data-key="<?php the_ID(); ?>"></div>
        </section>
      </article>
    <link rel="stylesheet" href="//comment.moe/dest/static/css/plus.css">
    <script src="//comment.moe/dest/static/js/build.js" charset="UTF-8"></script>
<?php get_footer(); ?>