<?php get_header(); ?>
      <header>
        <h1><a href="<?php bloginfo('wpurl'); ?>"><?php bloginfo('name'); ?></a> - <?php bloginfo('description');?></h1>
      </header>
      <nav>
        <span><a title="home page" class="" href="/">Home</a></span>
        <?php get_sidebar(); ?>
		<span><a title="link page" class="" href="/link">Friend</a></span>
      </nav>
      <article class="content">
        <section class="post">
          <ul class="listing">
            <!--年份<li class="listing-seperator">2017</li>-->
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <li class="listing-item">
              <time datetime="<?php the_time('Y-m-d') ?>"><?php the_time('Y年m月d日') ?></time>
              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </li>
            <?php endwhile; ?>
            <?php else : ?>
                <h2>文章不存在</h2>
            <?php endif; ?>
          </ul>
        </section>
        <p class="clearfix"><?php previous_posts_link('&lt;&lt; 查看新文章', 0); ?> <span class="float right"><?php next_posts_link('查看旧文章 &gt;&gt;', 0); ?></span></p>
      </article>
<?php get_footer(); ?>